#!/bin/bash

installror_function="
# Função com todos os procedimentos
# para atualizar um projeto ruby on rails
# Necessário estar na pasta do projeto para
# rodar este comando
function installror() {
    git_branch="\$1"

    git pull origin \$git_branch
    bundle install
    rake assets:precompile rails_env=production
    rake db:migrate RAILS_ENV=production
    sudo systemctl reload nginx
}"

if [[ ! -f ~/.bashrc ]]; then
    touch ~/.bashrc
fi

bash_content="$(cat ~/.bashrc)"

if [[ $bash_content =~ installror\(\) ]]; then
    echo 'Ooops...'
    echo "You're trying to install it again?"
    echo 'Open ~/.bashrc and remove the function installror() from this file before continuing.'
    echo 'See you later!'
    exit 1
fi

echo "$installror_function" >> ~/.bashrc

message='Instalation successfuly!

How to use:
   (1) go the the project folder and then
   (2) type "installror remove_branch_name"

Ex.: installror master

Almost done!
Please log out from the server and log in again to reload you profile.

Happy installing. :)
'

echo "$message"
